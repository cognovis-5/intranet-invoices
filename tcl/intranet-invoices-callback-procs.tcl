# packages/intranet-invoices/tcl/intranet-invoices-callback-procs.tcl

## Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#

ad_library {
    
    Callback procs
    
    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
    @creation-date 2011-04-21
    @cvs-id $Id$
}


ad_proc -public -callback intranet-invoices::reminder_send {
    -invoice_id:required
} {
    Callback that is executed once a reminder has been send.
} -

ad_proc -public -callback intranet-invoices::mail_before_send {
    -invoice_id:required
    -type_id:required
} {
    Callback that is executed before a quote is send. Useful for e.g. setting an accept_url.
} -

ad_proc -public -callback intranet-invoices::merge_filename_change {
    -invoice_id:required
    -file_extension:required
} {
    Callback to allow the changing of the filename
} -

ad_proc -public -callback acs_mail_lite::send -impl intranet-invoice_update_quote {
    -package_id:required
    -message_id:required
    -from_addr:required
    -to_addr:required
    -body:required
    {-mime_type "text/plain"}
    {-subject}
    {-cc_addr}
    {-bcc_addr}
    {-file_ids}
    {-object_id}
    {-filesystem_files}
} {
    Set the project to quote_out and set the quote itself to send
} {

    set quote_p [db_string quote_p "select 1 from im_costs where cost_id = :object_id and cost_type_id = [im_cost_type_quote]" -default 0]
    if {$quote_p} {
        # Update the quote to cost status Send
        db_dml update_cost_status "update im_costs set cost_status_id = 3815 where cost_id = :object_id and cost_status_id in (3802,3804)"
        
        set project_id [db_string project_id "select project_id from im_costs where cost_id = :object_id" -default ""]
        
        if {$project_id ne ""} {
            # Update the project to quote_out
            db_dml update_project "update im_projects set project_status_id = 75 where project_id = :project_id and project_status_id < 75"
        }
    }
}

ad_proc -public -callback im_invoices_def_comp_template {
    -cost_type_id
    -company_id
    {-cost_center_id ""}
} {
    Callback to enable custom procedure calls for the company template
} -

ad_proc -public -callback im_invoice_after_create -impl im_invoices_storno_corrected {
    -object_id
    -status_id
    -type_id
} {
    Callback to create a storno invoice in case of a correction
} {
    set invoice_id $object_id

    switch $type_id {
	3725 - 3735 {
	    # This is a correction, we need to create a cancellation invoice first
	    
	    # What is the linked invoice
	    set linked_invoice_ids [relation::get_objects -object_id_two $invoice_id -rel_type "im_invoice_invoice_rel"]
	    set linked_invoice_ids [concat [relation::get_objects -object_id_one $invoice_id -rel_type "im_invoice_invoice_rel"] $linked_invoice_ids]
	    if {[llength $linked_invoice_ids] ne 1} {
		# We don't know for which linked invoice this cancellation is, so do nothing
		ns_log Error "We could not create a cancellation for the created invoice [im_name_from_id $object_id]"
	    } else {
		set linked_invoice_id [lindex $linked_invoice_ids 0]
		# Create the cancellation invoice
		switch $type_id {
		    3725 { set target_cost_type_id [im_cost_type_cancellation_invoice] }
		    3735 { set target_cost_type_id [im_cost_type_cancellation_bill] }
		}

		im_invoice_copy_new -source_invoice_ids $linked_invoice_id -target_cost_type_id $target_cost_type_id
	    }
	}
    }
}

ad_proc -public -callback im_invoice_after_create -impl im_invoices_update_project_after_quote {
  -object_id
  -status_id
  -type_id
} {
    Callback to update project status to 'Quoting' after quote is created
} {
    
    ns_log Notice "Callback implementation im_invoices_update_project_after_quote for invoice_id $object_id"
    set quote_type_id [im_cost_type_quote]
    set inquiring_status_id 72
    # First we check if invoice type is quote
    if {$type_id eq $quote_type_id} {
        # Next we need to get project_id of that quote. Code won't continue execution if project_id is empty
        set invoice_linked_project_id [db_string get_invoice_linked_project_id "select project_id from im_costs where cost_id =:object_id" -default ""]
        if {$invoice_linked_project_id ne ""} {
            set linked_project_status_id [db_string get_linked_project_status_id "select project_status_id from im_projects where project_id=:invoice_linked_project_id" -default 0]
            # Next we make sure that project_id is inquiring
            if {$linked_project_status_id eq [im_project_status_inquiring]} {
		        set quoting_status_id [im_project_status_quoting]
		        db_dml update_project_status "update im_projects set project_status_id =:quoting_status_id where project_id =:invoice_linked_project_id"
            }
        }
    }
}

ad_proc -public -callback im_invoices::oo::before_render {
    -cost_type_id
    -invoice_id
    -current_user_id
} {
    Callback to enable custom procedure calls for the company template
} -