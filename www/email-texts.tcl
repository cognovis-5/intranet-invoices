# packages/intranet-invoices/www/email-texts.tcl
#
# Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
 
ad_page_contract {
    
    Page to quickly add the texts
    
    @author malte.sussdorff@cognovis.de
    @creation-date 2012-03-01
    @cvs-id $Id$
} {
    {locale "en_US"}
} -properties {
} -validate {
} -errors {
}

# We rename to avoid conflict in queries
set current_locale $locale
set default_locale en_US
set return_url [ad_return_url]

set locale_label [lang::util::get_label $current_locale]
set default_locale_label [lang::util::get_label $default_locale]
set package_key "intranet-invoices"

# Locale switch
set languages [lang::system::get_locale_options]

ad_form -name locale_form -action [ad_conn url] -export { tree_id category_id } -form {
	{locale:text(select) {label "Language"} {value $locale} {options $languages}}
}

set user_id [ad_conn user_id]
template::multirow create messages category translated_subject translated_body default_subject default_body default_subject_edit_url default_body_edit_url translated_subject_edit_url translated_body_edit_url


db_foreach category "select parent_id, category_id from im_categories left outer join im_category_hierarchy on (child_id = category_id) where category_type = 'Intranet Cost Type' order by category_id" {
	
	# get the data used in mails
	db_0or1row invoice_info "select *, company_contact_id as recipient_id from im_costs, im_invoices where cost_id = invoice_id and cost_id = (select max(cost_id) from im_costs where cost_type_id = :category_id)"	
	db_1row user_info "select first_names, last_name, im_name_from_user_id(:user_id) as internal_contact_name from persons where person_id = :recipient_id"
	
	set recipient_locale [lang::user::locale -user_id $recipient_id]
	set salutation_pretty [im_invoice_salutation -person_id $recipient_id]	
	callback intranet-invoices::mail_before_send -invoice_id $invoice_id -type_id $cost_type_id

	if {![db_0or1row related_projects_sql "
			select distinct
			r.object_id_one as project_id,
			p.project_name,
			project_lead_id,
			im_name_from_id(project_lead_id) as project_manager,
			p.project_nr,
			p.parent_id,
			p.description,
			trim(both p.company_project_nr) as customer_project_nr
		from
				acs_rels r,
			im_projects p
		where
			r.object_id_one = p.project_id
			and r.object_id_two = :invoice_id
			order by project_id desc
			limit 1
	"]} {
		set project_name ""
		set project_manager [im_name_from_user_id $user_id]
		set project_lead_id $user_id
		set customer_project_nr ""
		set project_nr ""
	}


    if {"" != $parent_id} {
		set parent "[im_category_from_id $parent_id] -- "
    } else {
		set parent ""
    }
    
    
    set category "$parent[im_category_from_id $category_id]"
	set default_subject [lang::message::lookup $default_locale intranet-invoices.invoice_email_subject_${category_id} "<span style='color: gray; font-style: italic;'>Not provided</span>"]
		set default_body [lang::message::lookup $default_locale intranet-invoices.invoice_email_body_${category_id} "<span style='color: gray; font-style: italic;'>Not provided</span>"]
	if {$default_subject eq "<span style='color: gray; font-style: italic;'>Not provided</span>"} {
		set translated_subject ""
	} else {
		set translated_subject [lang::message::lookup $current_locale intranet-invoices.invoice_email_subject_${category_id} "<span style='color: gray; font-style: italic;'>Not translated</span>"]	
	}
	if {$default_body eq "<span style='color: gray; font-style: italic;'>Not provided</span>"} {
		set translated_body ""
	} else {
	    set translated_body [lang::message::lookup $current_locale intranet-invoices.invoice_email_body_${category_id} "<span style='color: gray; font-style: italic;'>Not translated</span>"]
    }
    set default_subject_edit_url [export_vars -base "/acs-lang/admin/edit-localized-message" -url {{package_key "intranet-invoices"} {message_key "invoice_email_subject_${category_id}"} {locale $default_locale} return_url}]

    set default_body_edit_url [export_vars -base "/acs-lang/admin/edit-localized-message" -url {{package_key "intranet-invoices"} {message_key "invoice_email_body_${category_id}"} {locale $default_locale} return_url}]

	set translated_subject_edit_url [export_vars -base "/acs-lang/admin/edit-localized-message" -url {{package_key "intranet-invoices"} {message_key "invoice_email_subject_${category_id}"} {locale $current_locale} return_url}]
    
    set translated_body_edit_url [export_vars -base "/acs-lang/admin/edit-localized-message" -url {{package_key "intranet-invoices"} {message_key "invoice_email_body_${category_id}"} {locale $current_locale} return_url}]

    multirow append messages $category $translated_subject $translated_body $default_subject $default_body $default_subject_edit_url $default_body_edit_url $translated_subject_edit_url $translated_body_edit_url
}

set form_vars [export_ns_set_vars form {locale form:mode form:id __confirmed_p __refreshing_p formbutton:ok} [ad_conn form]]

set page_title "Email Texts"
set sub_navbar [im_costs_navbar "none" "/intranet/invoices/index" "" "" [list]] 
